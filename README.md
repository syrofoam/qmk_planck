# qmk_planck
Personal keyboard firmware written for Planck from drop.com


## Dependecies
Needs the headers and files from the QMK project.

- [Reuse code from my own Preonic firmware]
- [Remove some of the unwated subrutine]
- [ ] [Remove dipswitch support]
- [ ] [Remove knob support and set high polling rate]

```
clone https://github.com/qmk/qmk_firmware
cd into qmk_firmware/keyboards/planck/keymaps/
git clone https://gitlab.com/syrofoam/qmk_planck
rename qmk_planc to syrofoam
qmk compile -kb planck/rev6_drop -km syrofoam
put KB in DFU flash mode
qmk flash -kb planck/rev6_drop -km syrofoam
```

## License
GPL-2

## Project status
Compiles without errors.
